import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('Mobile/AddProduct/TC000 - login successful'), [:], FailureHandling.STOP_ON_FAILURE)

Mobile.pressBack()

Mobile.tap(findTestObject('Mobile/Mobile - add_product/OR001 - add_product_icon'), 10)

Mobile.verifyElementText(findTestObject('Mobile/Mobile - add_product/TXT001 - add_product_pageTitle'), addProductTitle)

Mobile.setText(findTestObject('Mobile/Mobile - add_product/OR002 - product_name_input'), productName, 0)

Mobile.setText(findTestObject('Mobile/Mobile - add_product/OR003 - product_price_input'), productPrice, 0)

Mobile.tap(findTestObject('Mobile/Mobile - add_product/OR004 - product_category_dropdownMenu'), 5)

Mobile.tap(findTestObject('Mobile/Mobile - add_product/OR005 - category_item_selectValue'), 5)

Mobile.setText(findTestObject('Mobile/Mobile - add_product/OR006 - location_input'), locationName, 0)

Mobile.setText(findTestObject('Mobile/Mobile - add_product/OR007 - description_textBox'), description, 0)

Mobile.tap(findTestObject('Mobile/Mobile - add_product/OR009 - upload_image_deviceMenu'), 0)

Mobile.tap(findTestObject('Mobile/Mobile - add_product/OR010 - gallery_deviceMenu'), 0)

Mobile.tap(findTestObject('Mobile/Mobile - add_product/OR012 - other_folder_deviceMenu'), 0)

Mobile.tap(findTestObject('Mobile/Mobile - add_product/OR013 - download_folder_deviceMenu'), 0)

Mobile.tap(findTestObject('Mobile/Mobile - add_product/PIC001 - Item_picture_image'), 0)

Mobile.tap(findTestObject('Mobile/Mobile - add_product/OR015 - publish_button'), 10)

Mobile.delay(5, FailureHandling.STOP_ON_FAILURE)

Mobile.verifyElementText(findTestObject('Mobile/Mobile - add_product/TXT008 - seller_item_list_pageTitle'), sellerItemList)

Mobile.verifyElementText(findTestObject('Mobile/Mobile - add_product/TXT009 - uploaded_itemName'), productName)

Mobile.takeScreenshot('successAddProduct', FailureHandling.STOP_ON_FAILURE)

Mobile.closeApplication()