import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

Mobile.startApplication('C:\\Users\\Melvin\\Katalon Studio\\app-release.apk', false)

Mobile.tap(findTestObject('Mobile/Mobile - login/OR001 - user_logo'), 5)

Mobile.tap(findTestObject('Mobile/Mobile - login/OR002 - pre_login_button'), 5)

Mobile.setText(findTestObject('Object Repository/Mobile/Mobile - login/OR003 - email_input'), email, 0)

Mobile.setText(findTestObject('Object Repository/Mobile/Mobile - login/OR004 - password_input'), password, 0)

Mobile.tap(findTestObject('Object Repository/Mobile/Mobile - login/OR005 - login_button'), 0)

Mobile.verifyElementText(findTestObject('Mobile/Mobile - login/TXT001 - my_account_text'), userPage)

Mobile.takeScreenshot('successLogin', FailureHandling.STOP_ON_FAILURE)

