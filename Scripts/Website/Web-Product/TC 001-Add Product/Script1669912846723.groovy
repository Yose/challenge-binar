import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.configuration.RunConfiguration as RunConfiguration

WebUI.callTestCase(findTestCase('Website/Web-Login/TC 001-User want to login using registered email and password'), [:], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.click(findTestObject('Website/Web-Add Product/Penawaran Produk'))

WebUI.click(findTestObject('Website/Web-Add Product/Tambah Produk'))

WebUI.setText(findTestObject('Website/Web-Add Product/input_Nama Produk'), 'smartphone')

WebUI.setText(findTestObject('Website/Web-Add Product/input_Harga Produk'), '6000000')

WebUI.selectOptionByValue(findTestObject('Website/Web-Add Product/Pilih Kategori'), '4', false)

WebUI.setText(findTestObject('Website/Web-Add Product/Deskripsi'), 'Smartphone')

String directory = RunConfiguration.getProjectDir()

pathFile = ((directory + '/images/') + 'smartphone.jpeg')

//String filePath = RunConfiguration.getProjectDir() + "/image/vario.jpeg"
WebUI.comment(pathFile)

WebUI.uploadFile(findTestObject('Website/Web-Add Product/upload file'), pathFile)

WebUI.delay(5)

WebUI.click(findTestObject('Website/Web-Add Product/button_Terbitkan'))

WebUI.delay(5)

WebUI.closeBrowser()

