#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@addProduct
Feature: Add product in secondhand app
  user want to add product in secondhand app

  
  @ADD001
  Scenario: user adds product with complete data
    Given Open Apps and redirect to secondhand apps
    When User click add product menu
    And User enter product name "Ram Corsair"
    And User enter product price "800000"
    And User click drop down category list
    And User select category product
    And User enter location "jakarta"
    And User enter description "Size / Capacity 16GB (2x8GB) DDR4"
    And User click upload image icon
    And User click gallery menu
    And User click list of folder
    And User click download folder
    And User select image product
    And User click publish button
    Then User verify success and direct to sell list page  "Daftar Jual Saya"
    And User verify uploaded product is correct "Ram Corsair"
