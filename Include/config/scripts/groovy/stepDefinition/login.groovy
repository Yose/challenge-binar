package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then

import internal.GlobalVariable

public class login {

	@Given("User open the Apps and redirect to secondhand apps")
	public void user_open_the_Apps_and_redirect_to_secondhand_apps() {

		Mobile.startApplication("C:\\Users\\Melvin\\Katalon Studio\\app-release.apk", false)
	}

	@When("User click account menu")
	public void user_click_account_menu() {

		Mobile.tap(findTestObject('Object Repository/Mobile - login/OR001 - user_logo'), 5)
	}

	@When("User click sign in button")
	public void user_click_sign_in_button() {

		Mobile.tap(findTestObject('Object Repository/Mobile - login/OR002 - pre_login_button'), 5)
	}

	@When("User enter email {string}")
	public void user_enter_email(String email) {

		Mobile.setText(findTestObject('Object Repository/Mobile - login/OR003 - email_input'), email, 0)
	}

	@When("User enter password {string}")
	public void user_enter_password(String password) {

		Mobile.setText(findTestObject('Object Repository/Mobile - login/OR004 - password_input'), password, 0)
	}

	@When("User click login button")
	public void user_click_login_button() {

		Mobile.tap(findTestObject('Object Repository/Mobile - login/OR005 - login_button'), 0)
	}

	@Then("Verify success login and direct to {string}")
	public void verify_success_login_and_direct_to(String userPage) {

		Mobile.verifyElementText(findTestObject('Object Repository/Mobile - login/TXT001 - my_account_text'), userPage)
	}
}
