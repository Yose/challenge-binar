package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.When
import cucumber.api.java.en.Then

import internal.GlobalVariable

public class Mobile_addProduct {

	@Given("Open Apps and redirect to secondhand apps")
	public void open_Apps_and_redirect_to_secondhand_apps() {

		// assumption : user is already log in
		// first method = this step will look like the user opened the app 2 times
		// Mobile.startApplication("C:\\Users\\Melvin\\Katalon Studio\\app-release.apk", false)

		// second method = this step will look better than the first method
		Mobile.pressBack()
	}

	@When("User click add product menu")
	public void user_click_add_product_menu() {

		Mobile.tap(findTestObject('Object Repository/Mobile/Mobile - add_product/OR001 - add_product_icon'), 10)
	}

	@When("User enter product name {string}")
	public void user_enter_product_name(String productName) {

		Mobile.setText(findTestObject('Object Repository/Mobile/Mobile - add_product/OR002 - product_name_input'), productName, 0)
	}

	@When("User enter product price {string}")
	public void user_enter_product_price(String productPrice) {

		Mobile.setText(findTestObject('Object Repository/Mobile/Mobile - add_product/OR003 - product_price_input'), productPrice, 0)
	}

	@When("User click drop down category list")
	public void user_click_drop_down_category_list() {

		Mobile.tap(findTestObject('Object Repository/Mobile/Mobile - add_product/OR004 - product_category_dropdownMenu'), 5)
	}

	@When("User select category product")
	public void user_select_category_product() {

		Mobile.tap(findTestObject('Object Repository/Mobile/Mobile - add_product/OR005 - category_item_selectValue'), 5)
	}

	@When("User enter location {string}")
	public void user_enter_location(String locationName) {

		Mobile.setText(findTestObject('Object Repository/Mobile/Mobile - add_product/OR006 - location_input'), locationName, 0)
	}

	@When("User enter description {string}")
	public void user_enter_description(String description) {

		Mobile.setText(findTestObject('Object Repository/Mobile/Mobile - add_product/OR007 - description_textBox'), description, 0)
	}

	@When("User click upload image icon")
	public void user_click_upload_image_icon() {

		Mobile.tap(findTestObject('Object Repository/Mobile/Mobile - add_product/OR008 - upload_image'), 0)
	}

	@When("User click gallery menu")
	public void user_click_gallery_menu() {

		Mobile.tap(findTestObject('Object Repository/Mobile/Mobile - add_product/OR010 - gallery_deviceMenu'), 0)
	}

	@When("User click list of folder")
	public void user_click_list_of_folder() {

		Mobile.tap(findTestObject('Object Repository/Mobile/Mobile - add_product/OR012 - other_folder_deviceMenu'), 0)
	}

	@When("User click download folder")
	public void user_click_download_folder() {

		Mobile.tap(findTestObject('Object Repository/Mobile/Mobile - add_product/OR013 - download_folder_deviceMenu'), 0)
	}

	@When("User select image product")
	public void user_select_image_product() {

		Mobile.tap(findTestObject('Object Repository/Mobile/Mobile - add_product/PIC001 - Item_picture_image'), 0)
	}

	@When("User click publish button")
	public void user_click_publish_button() {

		Mobile.tap(findTestObject('Object Repository/Mobile/Mobile - add_product/OR015 - publish_button'), 10)
	}

	@Then("User verify success and direct to sell list page  {string}")
	public void user_verify_success_and_direct_to_sell_list_page(String sellerItemList) {

		Mobile.delay(5)
		Mobile.verifyElementText(findTestObject('Object Repository/Mobile/Mobile - add_product/TXT008 - seller_item_list_pageTitle'), sellerItemList)
	}

	@Then("User verify uploaded product is correct {string}")
	public void user_verify_uploaded_product_is_correct(String productName) {

		Mobile.verifyElementText(findTestObject('Object Repository/Mobile/Mobile - add_product/TXT009 - uploaded_itemName'), productName)
	}
}
