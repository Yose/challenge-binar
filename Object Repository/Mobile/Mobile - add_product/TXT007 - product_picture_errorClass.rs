<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>TXT007 - product_picture_errorClass</name>
   <tag></tag>
   <elementGuidId>f588d171-bd6c-46fc-a168-aeaef756a5ae</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.Toast</value>
      <webElementGuid>019df7ed-de1a-4e29-abe2-db7f5f570e3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>e878698e-071f-4927-9340-4489fa423722</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Silahkan masukkan foto produk terlebih dahulu.</value>
      <webElementGuid>c6a3d489-d553-45ca-9fab-c67f458fc155</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>package</name>
      <type>Main</type>
      <value>com.android.settings</value>
      <webElementGuid>fcbde427-3619-416a-b6fa-ec9f94ebc5f7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>displayed</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>e4e5f3b2-07dd-42da-9b05-34023f378885</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//hierarchy/android.widget.Toast[1]</value>
      <webElementGuid>feaaeeec-bda0-4b38-ba17-721dca930b04</webElementGuid>
   </webElementProperties>
   <locator>//*[(@text = 'Silahkan masukkan foto produk terlebih dahulu.' or . = 'Silahkan masukkan foto produk terlebih dahulu.')]</locator>
   <locatorStrategy>ATTRIBUTES</locatorStrategy>
</MobileElementEntity>
