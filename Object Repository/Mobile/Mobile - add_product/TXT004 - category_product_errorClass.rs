<?xml version="1.0" encoding="UTF-8"?>
<MobileElementEntity>
   <description></description>
   <name>TXT004 - category_product_errorClass</name>
   <tag></tag>
   <elementGuidId>821eaa8f-8130-49b2-b1d0-49ed2e223662</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>android.widget.Toast</value>
      <webElementGuid>ad47481a-00c6-446a-84e6-78c11d5c1812</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>index</name>
      <type>Main</type>
      <value>1</value>
      <webElementGuid>0357a454-8630-46fd-b376-a6c82eee5c5a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Pilih minimal 1 kategori</value>
      <webElementGuid>28a781e7-d6ba-469c-8dd8-fd0f505b1411</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>package</name>
      <type>Main</type>
      <value>com.android.settings</value>
      <webElementGuid>67b5cb32-3618-4d2b-87fa-701398add92b</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>displayed</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>af0f38b1-91dd-433d-9047-c2d8ccf1da63</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//hierarchy/android.widget.Toast[1]</value>
      <webElementGuid>343596ed-24c3-4abb-8434-4ffcd1048d4a</webElementGuid>
   </webElementProperties>
   <locator>//*[@class = 'android.widget.Toast']</locator>
   <locatorStrategy>ATTRIBUTES</locatorStrategy>
</MobileElementEntity>
